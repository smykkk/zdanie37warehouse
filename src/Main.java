import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner skan = new Scanner(System.in);
        String inputLine = "";
        boolean isWorking = true;
        Warehouse warehouse = new Warehouse();

        while (isWorking){
            printOptions();
            inputLine = skan.nextLine();
            try {
                if(inputLine.toLowerCase().contains("quit")){
                    isWorking = false;
                }
                String[] inputArguments = inputLine.split(" ");
                if (inputArguments[0].equals("add")) {
                    try {
                        String name = inputArguments[1];
                        Double price = Double.parseDouble(inputArguments[2]);
                        ProductType PRODUCTTYPE = ProductType.valueOf(inputArguments[3].toUpperCase());
                        ProductClass PRODUCTCLASS = ProductClass.valueOf(inputArguments[4].toUpperCase());
                        warehouse.addProduct(name, price, PRODUCTCLASS, PRODUCTTYPE);
                        System.out.println("Product added.");
                    } catch (NumberFormatException nfe) {
                        System.out.println("Wrong price format entered.");
                    } catch (IllegalArgumentException iae) {
                        System.out.println("Wrong product type or class.");
                    }
                } else if (inputArguments[0].equals("checkType")) {
                    try {
                        String name = inputArguments[1];
                        ProductType PRODUCTTYPE = ProductType.valueOf(inputArguments[2].toUpperCase());
                        if (warehouse.checkProductExistence(name)) {
                            System.out.println("Product " + name + " is of type " + PRODUCTTYPE + ":" + warehouse.checkType(name, PRODUCTTYPE));
                        } else System.out.println("Product does not exist.");
                    } catch (IllegalArgumentException iae) {
                        System.out.println("Wrong productType given.");
                    }
                } else if (inputArguments[0].equals("checkClass")){
                    try {
                        String name = inputArguments[1];
                        ProductClass PRODUCTCLASS = ProductClass.valueOf(inputArguments[2].toUpperCase());
                        if (warehouse.checkProductExistence(name)) {
                            System.out.println("Product " + name + " is of type " + PRODUCTCLASS + ":" + warehouse.checkClass(name, PRODUCTCLASS));
                        } else System.out.println("Product does not exist.");
                    } catch (IllegalArgumentException iae) {
                        System.out.println("Wrong productClass given.");
                    }
                } else if(inputArguments[0].equals("printClass")){
                    try {
                        ProductClass PRODUCTCLASS = ProductClass.valueOf(inputArguments[1].toUpperCase());
                        System.out.println(warehouse.getProductMapByClass().get(PRODUCTCLASS));
                    }catch (IllegalArgumentException iae){
                        System.out.println("Wrong productClass given.");
                    }
                } else if(inputArguments[0].equals("printType")){
                    try {
                        ProductType PRODUCTTYPE = ProductType.valueOf(inputArguments[1].toUpperCase());
                        System.out.println(warehouse.getProductMapByType().get(PRODUCTTYPE));
                    }catch (IllegalArgumentException iae){
                        System.out.println("Wrong productType given.");
                    }
                } else if(inputArguments[0].equals("searchProduct")){
                    String name = inputArguments[1];
                    System.out.println("Product " + name + "exists: " + warehouse.checkProductExistence(name));
                }
            }catch (ArrayIndexOutOfBoundsException aioobe){
                System.out.println("Not enough information given.");
            }
            System.out.println();
        }

    }

    private static void printOptions(){
        System.out.println("add (product): ");
        System.out.println("Name, price, type[ALCOHOL, FOOD, INDUSTRIAL], productClass[HIGH, MID, LOW]");
        System.out.println("______________");
        System.out.println("checkType / checkClass: ");
        System.out.println("name + type/class");
        System.out.println("______________");
        System.out.println("printType / printClass: ");
        System.out.println("type / class");
        System.out.println("______________");
        System.out.println("searchProduct: ");
        System.out.println("name");
    }
}
