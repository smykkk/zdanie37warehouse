public class Product {
    String name;
    Double price;
    ProductType type;
    ProductClass productClass;

    public Product(String name, Double price, ProductType type, ProductClass productClass) {
        this.name = name;
        this.price = price;
        this.type = type;
        this.productClass = productClass;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public ProductType getType() {
        return type;
    }

    public ProductClass getProductClass() {
        return productClass;
    }
}
